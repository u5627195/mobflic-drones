# A solution written in scala to solve the following challenge:
---
A group of drones are going to be landed by the Mobflic team on a platform in a park.

This platform is rectangular and must be navigated by the drones so that their onboard drone cameras can get a comprehensive view of the surrounding park to send back to Mobflic team.

A drone’s position is represented by a combination of an x and y co-ordinates and a letter representing one of the four cardinal compass points. The platform is divided up into a grid to simplify navigation. An example position might be 0, 0, N, which means the drone is in the bottom left corner and facing North.

In order to control a drone, the Mobflic team sends a simple string of letters. The possible letters are 'L', 'R' and 'M'. 'L' and 'R' makes the drone spin 90 degrees left or right respectively, without moving from its current spot.

'M' means move forward one grid point, and maintain the same heading.

Assume that the square directly North from (x, y) is (x, y+1).

### Input:

The first line of input is the upper-right coordinates of the platform, the lower-left coordinates are assumed to be 0,0.

The rest of the input is information pertaining to the drones that have been deployed. Each drone has two lines of input. The first line gives the drone’s position, and the second line is a series of instructions telling the drone how to explore the platform.

The position is made up of two integers and a letter separated by spaces, corresponding to the x and y co-ordinates and the drone’s orientation.

Each drone will be finished sequentially, which means that the second drone won't start to move until the first one has finished moving.

### Output:

The output for each drone should be its final co-ordinates and heading.

### Test Input:
```
5 5
1 2 N
LMLMLMLMM
3 3 E
MMRMMRMRRM
```
---
### Expected Output:
```
1 3 N
5 1 E
```