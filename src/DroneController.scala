/**
  * Created by thomas on 12/12/16.
  */

import scala.io.Source

object DroneController {

  var xSize = 0
  var ySize = 0

  def main(args: Array[String]): Unit = {

    val lines = Source.fromFile("testInput.txt" ).getLines.toList

    var parser = new InputParser(lines)

    var droneList:List[Drone] = parser.parseInstructions

    for( drone <- droneList ) println(drone.toString)

  }
}
