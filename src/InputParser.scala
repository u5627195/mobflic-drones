import scala.collection.mutable.{ArrayBuffer, Queue}

/**
  * Created by thomas on 12/12/16.
  */
class InputParser (lines:List[String]) {
  var queue = Queue(lines: _*)

  val dimensions:Array[String] = queue.dequeue().split(" ")
  DroneController.xSize = dimensions(0).toInt
  DroneController.ySize = dimensions(1).toInt

  def parseInstructions:List[Drone] = {

    var droneList = new ArrayBuffer[Drone]

    while(!queue.isEmpty) {
      var position = queue.dequeue()
      var instructions = queue.dequeue()
      droneList += new Drone(position, instructions)
    }

    droneList.toList

  }

}
