
import scala.collection.mutable

/**
  * Created by thomas on 12/12/16.
  */
class Drone (startingPosition: String, instructions: String) {

  val positionArray = startingPosition.split(" ")
  val orientationList:List[String] = List("N","E","S","W")

  var orientationIndex = 0
  var xPos = 0
  var yPos = 0

  parseParameters()
  processInstructions()


  def parseParameters() = {

    xPos = positionArray(0).toInt
    yPos = positionArray(1).toInt
    orientationIndex = orientationList.indexOf(positionArray(2))
  }

  def processInstructions(): Unit = {
    for (c <- instructions) {
      c match {
        case 'L' => orientationIndex = ((orientationIndex - 1) + 4) % 4
        case 'R' => orientationIndex = (orientationIndex + 1) % 4
        case 'M' => moveForward(orientationList(orientationIndex))
        case _ => "Invalid instruction" // the default, catch-all
      }
    }
  }

  def moveForward(orientation: String) = {
    orientation match {
      case "N" => yPos += 1
      case "S" => yPos -= 1
      case "E" => xPos += 1
      case "W" => xPos -= 1
    }
  }

  override def toString: String = xPos + " " + yPos + " " + orientationList(orientationIndex)

}
